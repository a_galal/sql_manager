import org.junit.Test;
import org.sqlmanager.SQLManager;

import java.io.IOException;

import static org.junit.Assert.*;

public class SQLManagerTest {

    public static final String PKG_WITH_VALID_SQL = "org.sqlmanager.valid";
    public static final String PKG_WITH_INVALID_SYNTAX = "org.sqlmanager.invalid.syntax";
    public static final String PKG_WITH_EMPTY_FILE = "org.sqlmanager.empty";

    @Test
    public void testGetSqlFiles() throws IOException {
        //String pkgName= this.getClass().getPackage().getName();

        SQLManager mgr = SQLManager.getInstance(PKG_WITH_VALID_SQL);

        assertEquals(3, mgr.sqlFiles.size());
    }

    @Test
    public void testReadingSqlQueries() throws IOException {
        SQLManager mgr = SQLManager.getInstance(PKG_WITH_VALID_SQL);
        assertEquals("\nselect * from test_table", mgr.getQuery("sql1"));
    }

    @Test
    public void testReadingEmptyFile() throws IOException {
        SQLManager mgr = SQLManager.getInstance(PKG_WITH_EMPTY_FILE);
        assertTrue(mgr.hasNoQueries());
    }

    @Test(expected = IllegalStateException.class)
    public void testReadingInvalidSyntax() throws IOException {
        SQLManager mgr = SQLManager.getInstance(PKG_WITH_INVALID_SYNTAX);
    }
}
