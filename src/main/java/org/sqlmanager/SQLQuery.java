package org.sqlmanager;

import java.util.Objects;

public class SQLQuery {
    private String name;
    private String sql;


    public SQLQuery(String name, String sql) {
        this.name = name;
        this.sql = sql;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SQLQuery sqlQuery = (SQLQuery) o;
        return Objects.equals(name, sqlQuery.name) &&
                Objects.equals(sql, sqlQuery.sql);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, sql);
    }
}
