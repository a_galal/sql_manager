package org.sqlmanager;

import com.google.common.io.Resources;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SQLManager {

    public static final String FILE_NAME_REGEX = ".*\\.sql$";


    //(--@[\S]+).* -> extract the query name as the first word following '--@'
    //and ignore the rest of the line
    // , ex: --@name   bla bla bla bla  -> then the regex should extract "--@name" only

    //[\r\n] -> starting from next line
    //([\s\S]+);* -> any thing written until you meet a ';' is the query sql.
    public static final String QUERY_EXTRACT_REGEX="(--@[\\S]+).*[\\r\\n]([\\s\\S]+);*";


    public List<URL> sqlFiles = new ArrayList<URL>();
    private Map<String,String> sqlQueriesCache ;
    private Pattern queryExtractPattern ;

    private SQLManager(){

        queryExtractPattern = Pattern.compile(QUERY_EXTRACT_REGEX);
        sqlQueriesCache = new HashMap<>();
    }

    //=================================================================================

    static public SQLManager getInstance(String packageName) throws IOException {
        SQLManager mgr = new SQLManager();

        mgr.cacheSqlInResourcesFiles(packageName);

        return mgr;
    }

    //=================================================================================

    public  String getQuery(String queryName){

        return sqlQueriesCache.get(queryName);
    }

    //=================================================================================

    public Boolean hasNoQueries(){
        return sqlQueriesCache.isEmpty();
    }

    //=================================================================================

    private void cacheSqlInResourcesFiles(String packageName) throws IOException {
        sqlFiles = getSqlFiles(packageName);
        for(URL url : sqlFiles)
            cacheSqlInFile(url);
    }

    //=================================================================================

    private List<URL> getSqlFiles(String packageName) {

        Reflections reflections = new Reflections(packageName, new ResourcesScanner());
        Set<String> paths = reflections.getResources(Pattern.compile(FILE_NAME_REGEX));
        ClassLoader loader = getClass().getClassLoader();
        return paths.stream()
                .map(loader::getResource)
                .collect(Collectors.toList());
    }

    //=================================================================================

    private void cacheSqlInFile(URL url) throws IOException {
        List<String> queries = null;

        queries = splitFileContentBySemicolon(url);
        for(String query : queries)
            cacheQuery(query);
    }

    //=================================================================================

    private SQLQuery getFromRawString(String rawQueryString) throws IllegalStateException{
        String processedQueryStr = prepareRawQuery(rawQueryString);
        if(!isValidRawQuery(processedQueryStr))
            throw new IllegalStateException("Illegal named query syntax : \n" +rawQueryString);

        Matcher matcher = queryExtractPattern.matcher(processedQueryStr);
        matcher.find();
        String queryName = processQueryName(matcher.group(1));
        String sql = matcher.group(2);
        return new SQLQuery( queryName, sql);
    }

    //=================================================================================

    private String processQueryName(String queryName) {
        return queryName.replaceFirst("--@","");
    }

    //=================================================================================
    /**
     * @return the raw query after removing any extra spaces or characters before the "--"
     * */
    private String prepareRawQuery(String rawQueryString) {
        int startIndx = rawQueryString
                            .indexOf("--@",0);

        if(startIndx < 0)
            return rawQueryString;

        String processed = rawQueryString
                                    .substring(startIndx)
                                    .trim();

        return processed;
    }

    //=================================================================================

    private boolean isValidRawQuery(String rawQueryString) {
        return queryExtractPattern.matcher(rawQueryString).matches();
    }

    //=================================================================================

    private  void cacheQuery(String rawQueryString) {
        SQLQuery query = getFromRawString(rawQueryString);
        if(isValidAnnotation(query.getName()))
            sqlQueriesCache.put(query.getName() , query.getSql());
    }

    //=================================================================================

    private static boolean isValidAnnotation(String queryAnnotation) {
        return queryAnnotation != null && !queryAnnotation.isEmpty();
    }

    //=================================================================================


    private static List<String> splitFileContentBySemicolon(URL url) throws IOException {
        String content =  Resources.toString(url, StandardCharsets.UTF_8);
        List<String> parts = new ArrayList<>();
        for(String part : content.split(";"))
            if(!part.isEmpty())
                parts.add(part);
        return parts;
    }



}
