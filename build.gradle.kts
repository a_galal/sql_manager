group = "org.sqlmanager"
version = "1.0-SNAPSHOT"

plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

dependencies {
    compile("org.reflections:reflections:0.9.11")

    testCompile("junit", "junit", "4.12")

}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}